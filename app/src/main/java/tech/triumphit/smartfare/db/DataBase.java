package tech.triumphit.smartfare.db;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Tushar on 3/23/2018.
 */

@Database(name = DataBase.NAME, version = DataBase.VERSION)
public class DataBase{
    public static final String NAME = "SmartFare";
    public static final int VERSION = 2;
}
