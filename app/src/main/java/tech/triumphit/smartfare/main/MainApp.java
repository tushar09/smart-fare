package tech.triumphit.smartfare.main;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by Tushar on 3/23/2018.
 */

public class MainApp extends Application{
    @Override
    public void onCreate(){
        super.onCreate();
        FlowManager.init(new FlowConfig.Builder(this).build());
    }
}
