package tech.triumphit.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tushar on 3/23/2018.
 */

public class Constants{

    public static DBHelper getDbHelper(){
        return new DBHelper();
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
