package tech.triumphit.utils;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import tech.triumphit.smartfare.db.Users;
import tech.triumphit.smartfare.db.Users_Table;

/**
 * Created by Tushar on 3/23/2018.
 */

public class DBHelper{
    public boolean isEmailExists(String email){
        Users u = SQLite.select().from(Users.class).where(Users_Table.email.eq(email)).querySingle();
        if(u == null){
            return false;
        }else return true;
    }

    public boolean isUserExists(String email, String pass){
        Users u = SQLite.select().from(Users.class).where(Users_Table.email.eq(email), Users_Table.password.eq(pass)).querySingle();
        if(u == null){
            return false;
        }else return true;
    }
}
