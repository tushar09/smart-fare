package tech.triumphit.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import tech.triumphit.smartfare.R;
import tech.triumphit.smartfare.databinding.ActivityLoginBinding;
import tech.triumphit.smartfare.db.Users;
import tech.triumphit.utils.Constants;

public class LoginActivity extends AppCompatActivity{

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        binding.container.btLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(Constants.getDbHelper().isUserExists(binding.container.etEmail.getText().toString(), binding.container.etPass.getText().toString())){
                    Toast.makeText(LoginActivity.this, "Successfully logged in", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LoginActivity.this, MapsActivity.class));
                }else {
                    Toast.makeText(LoginActivity.this, "Invalid email and password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.container.btSignup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_settings){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Subscribe(sticky = true)
    public void onUserCreated(Users users) {
        binding.container.etEmail.setText(users.getEmail());
        binding.container.etPass.setText("123456");
        Toast.makeText(this, "Hey, my message " + users.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
