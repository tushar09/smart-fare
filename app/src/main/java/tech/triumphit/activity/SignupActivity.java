package tech.triumphit.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import tech.triumphit.smartfare.R;
import tech.triumphit.smartfare.databinding.ActivitySignupBinding;
import tech.triumphit.smartfare.db.Users;
import tech.triumphit.utils.Constants;

public class SignupActivity extends AppCompatActivity{

    private ActivitySignupBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        binding.container.etEmail.setText(System.currentTimeMillis() + "@tushar.com");
        binding.container.btSignup.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(!binding.container.etName.getText().equals("")){
                    if(!binding.container.etEmail.getText().equals("")){
                        if(!binding.container.etPass.getText().equals("")){
                            if(!binding.container.etPassConfirm.getText().equals("")){
                                if(Constants.isEmailValid(binding.container.etEmail.getText().toString())){
                                    if(binding.container.etPass.getText().toString().equals(binding.container.etPassConfirm.getText().toString())){
                                        if(Constants.getDbHelper().isEmailExists(binding.container.etEmail.getText().toString()) == false){
                                            Users user = new Users();
                                            user.setEmail(binding.container.etEmail.getText().toString());
                                            user.setPassword(binding.container.etPass.getText().toString());
                                            user.setName(binding.container.etName.getText().toString());
                                            user.save();
                                            EventBus.getDefault().postSticky(user);
                                            finish();
                                        }else {
                                            Toast.makeText(SignupActivity.this, "Email already exists", Toast.LENGTH_SHORT).show();
                                        }
                                    }else {
                                        Toast.makeText(SignupActivity.this, "Password did not match", Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    Toast.makeText(SignupActivity.this, "Please insert valid email", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(SignupActivity.this, "Please confirm password", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(SignupActivity.this, "Please insert password", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(SignupActivity.this, "Please insert email", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(SignupActivity.this, "Please insert name", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

}
